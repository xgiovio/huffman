#include "DataSet.h"

#include <iostream>


DataSet::DataSet(char in_character, float in_frequency,HElement * in_t_list)
{
	character = in_character;
	frequency = in_frequency;
	t_list = in_t_list;
}


DataSet::~DataSet(void)
{
	// bisogna deallocare la roba in t_list
}


char DataSet::getcharacter(){
	return character;
}
float DataSet::getfrequency(){
	return frequency;
}

void DataSet::increment_frequency(){
	++frequency;
}

HElement * DataSet::gett_list(){
	return t_list;
}

void DataSet::operator= (DataSet& in){
	character = in.getcharacter();
	frequency = in.getfrequency();
	t_list = in.gett_list();

}

bool DataSet::operator< (DataSet &in){
	if ( this->frequency < in.getfrequency() ){
		return true;	
	} else {
		return false;
	}
}


bool DataSet::order_dec (DataSet &a, DataSet &b){
		return !(a < b ); 
}

bool DataSet::operator==(const char& in){
	if (this->getcharacter() == in)
		return true;
	return false;
}
