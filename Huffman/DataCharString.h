#pragma once

#include <string>

class DataCharString
{
public:
	DataCharString(char a, std::string in_s);
	virtual ~DataCharString(void);

	std::string getCode ();
	char getChar ();

	bool operator< (DataCharString & in);

private:

	char character;
	std::string code;
};

