#pragma once

#include <cstdlib>
class HElement;

class DataSet
{
public:
	
	DataSet(char in_character, float in_frequency, HElement * in_t_list = NULL);
	
	virtual ~DataSet(void);

	char getcharacter();
	float getfrequency();
	void increment_frequency();
	HElement * gett_list();

	void operator= (DataSet& in);
	bool operator==(const char& in);

	bool operator< (DataSet &in);

	static bool order_dec (DataSet &a, DataSet &b);

private:
	char character;
	float frequency;
	HElement * t_list;
};




