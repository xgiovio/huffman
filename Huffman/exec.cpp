#include <vector>
#include <iostream>
#include <string>
#include <ctime>
#include <fstream>
#include <algorithm>

#include "DataSet.h"
#include "HElement.h"
#include "Huffman.h"


int main(int n_parameters, char** parameters) {


	std::vector<DataCharString> table_code;
	huffman_p (huffman_f ( read_data (*(parameters + 1) ) ), &table_code);
	
	std::sort (table_code.begin(), table_code.end());

	for (std::vector<DataCharString>::iterator i = table_code.begin() ; i!= table_code.end() ; ++i){
		std::cout << "Char: " << (*i).getChar() << " - Code: " << (*i).getCode() << std::endl;
	}


	std::cin.get();


	return 0;

}