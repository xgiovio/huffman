#pragma once

#include "HElement.h"
#include <vector>
#include <cstdlib>
#include "DataCharString.h"

HElement * huffman_f(std::vector <DataSet> in);
void huffman_p (HElement * list, std::vector <DataCharString> * vect , std::string in_s = "");
std::vector<DataSet> & read_data (char* file);
