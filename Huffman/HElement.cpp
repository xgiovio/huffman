#include "HElement.h"


HElement::HElement(bool status, DataSet left , DataSet right)
	:core(DataSet(0,0))
{
	concrete = status;
	if (left.gett_list() == NULL){
		left_p = new HElement(true,left);
	}else{
		left_p = left.gett_list();
	}


	if (right.gett_list() == NULL){
		right_p = new HElement(true,right);
	}else{
		right_p = right.gett_list();
	}

}

HElement::HElement(bool status, DataSet in_core )
	:core(in_core)
{
	concrete = status;
	left_p = right_p = NULL;

}


HElement::HElement(DataSet single)
	:core(DataSet(0,0))
{
	concrete = false;
	left_p = new HElement(true,single);
	right_p = NULL;
}


bool HElement::isconcrete(){
		return concrete;
	}


HElement * HElement::getleft_p (){
	return left_p;		
}

HElement * HElement::getright_p (){
	return right_p;		
}


DataSet HElement::getcore(){
	return core;
}


HElement::~HElement(void)
{
}
