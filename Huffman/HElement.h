#pragma once

#include "DataSet.h"

class HElement
{
public:
	HElement(bool status, DataSet left , DataSet right );
	HElement(bool status, DataSet in_core );
	HElement(DataSet single);

	virtual ~HElement(void);

	bool isconcrete();
	HElement * getleft_p ();
	HElement * getright_p ();
	DataSet getcore();

private:
	bool concrete;
	HElement * left_p;
	HElement * right_p;
	DataSet core;
};

