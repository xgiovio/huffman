#include "DataCharString.h"


DataCharString::DataCharString(char a, std::string in_s)
{
	character = a;
	code = in_s;
}

std::string DataCharString::getCode (){
	return code;
}

char DataCharString::getChar (){
	return character;
}

bool DataCharString::operator< (DataCharString & in){

	if (code.length() < in.getCode().length())
		return true;
	return false;

}


DataCharString::~DataCharString(void)
{
}
